﻿using System;
using System.Runtime.InteropServices;

namespace SyslogNet.client
{
    [ComVisible(true)]
    [Guid("9ddde4db-475e-43bc-90a0-ad6080301511")]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface ISyslogClient
    {
        void Open(string connectionURI);
        
        string AppName { get; set; }
        string CertificatePath { get; set; }
        string Host { get; }
        string Protocol { get; set; }
        int Port { get; set; }

        void Send3164(int facility, int severity, string message);
        void Send5424(int facility, int severity, string procid, string msgid, string message);
    }
}