using System;
using System.Collections.Generic;
using SyslogNet.Client.Serialization;
using System.Runtime.InteropServices;

namespace SyslogNet.Client.Transport
{
    [ComVisible(false)]
    public interface ISyslogMessageSender : IDisposable
	{
		void Reconnect();
		void Send(SyslogMessage message, ISyslogMessageSerializer serializer);
		void Send(IEnumerable<SyslogMessage> messages, ISyslogMessageSerializer serializer);
	}
}