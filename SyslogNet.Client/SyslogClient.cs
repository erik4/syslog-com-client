﻿using SyslogNet.Client;
using SyslogNet.Client.Serialization;
using SyslogNet.Client.Transport;
using System;
using System.EnterpriseServices;
using System.Runtime.InteropServices;


namespace SyslogNet.client
{
    [ComVisible(true)]
    [Guid("42416328-72e4-4954-b8ac-810b8fd836ef")]
    [ClassInterface(ClassInterfaceType.None)]
    [ProgId("SyslogClient")]
    [Synchronization(SynchronizationOption.Disabled)]
    public class SyslogClient : ISyslogClient
    {
        // some defaults
        private ISyslogMessageSender sender;
        private ISyslogMessageSerializer serializer;

        public SyslogClient() { }
        public void Open(string connectionURI)
        {
            Uri myUri = new Uri(connectionURI);
            if(String.IsNullOrEmpty(myUri.Host) || String.IsNullOrEmpty(myUri.Scheme) || myUri.Port < 0)
            {
                throw new ArgumentException("The connection URI should contain a scheme, a host and optionally a port, i.e.: 'udp://localhost:514'.");
            };

            host = myUri.Host;
            protocol = myUri.Scheme;
            port = myUri.Port == -1 ? 514 : myUri.Port;

            if (protocol.ToLower() == "tcp") { sender = (ISyslogMessageSender)new SyslogEncryptedTcpSender(Host, port); }
            else if (protocol.ToLower() == "udp") { sender = (ISyslogMessageSender)new SyslogUdpSender(Host, port); }
            else { throw new ArgumentException(message: "Protocol should be either tcp or udp"); }
        }

        public string CertificatePath { get; set; }
        public string AppName { get; set; }

        private string host = "localhost";
        public string Host
        {
            get
            {
                return host;
            }
        }

        private string protocol = "udp";
        public string Protocol
        {
            get
            {
                return protocol;
            }
            set
            {
                if (value.ToLower() == "tcp") { sender = (ISyslogMessageSender)new SyslogEncryptedTcpSender(Host, port); }
                else if (value.ToLower() == "udp") { sender = (ISyslogMessageSender)new SyslogUdpSender(Host, port); }
                else { throw new ArgumentException(message: "Protocol should be either tcp or udp"); }
                protocol = value;
            }
        }

        private int port = 514;
        public int Port
        {
            get
            {
                return port;
            }
            set
            {
                port = value;
            }
        }

        public void Send3164(int facility, int severity, string message)
        {
            serializer = (ISyslogMessageSerializer)new SyslogRfc3164MessageSerializer();
            SyslogMessage msg = new SyslogMessage(dateTimeOffset: null, appName: AppName, hostName: Host, facility: (Facility)facility, severity: (Severity)severity, message: message);
            sender.Send(msg, serializer);
        }

        public void Send5424(int facility, int severity, string procid, string msgid, string message)
        {
            serializer = (ISyslogMessageSerializer)new SyslogRfc5424MessageSerializer();
            SyslogMessage msg = new SyslogMessage(dateTimeOffset: null, appName: AppName, hostName: Host, facility: (Facility)facility, severity: (Severity)severity, procId: procid, msgId: msgid, message: message);
            sender.Send(msg, serializer);
        }

    }
}
