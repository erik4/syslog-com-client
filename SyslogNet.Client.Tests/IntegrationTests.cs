using System.Collections.Generic;
using Xunit;

namespace SyslogNet.Client.Tests
{
	public class IntegrationTests
	{
		[Fact]
		public void SendASimpleSyslogMessageToLocalhost()
		{
            var syslog = new SyslogNet.client.SyslogClient();
            syslog.Open("udp://localhost:514");
            syslog.Send5424(1, 3, "ASP", "APP", "This is a simple 5424 test to localhost");
            syslog.Send3164(1, 3, "This is a simple 3164 test to localhost");
        }

	}
}