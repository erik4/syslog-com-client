# SyslogNet ActiveX/COM+ client

This is a fork of SyslogNet by Andrew Smith.

It allows you to log to a syslog server from Classic ASP, other COM+ clients should also be able to do so using the provided DDLs, but that is untested.

Using this software you can use Syslog in Classic ASP for logging to Syslog servers. One of the advantages in using syslog is because it will allow you to easily log from IIS inside a docker container. IIS doesn't have a default solution for logging (especially from classic ASP) from inside a docker container. 

Please note that the original SyslogNet client is both targeted at .NET Framework 4 as well as .NET standard 2. Because ActiveX/COM+ is not available on .NET standard, this target is removed from this forked version. Also, sending to the local syslog was removed, because this only applies to Linux/Unix/BSD type systems.

### SyslogNet

.Net Syslog client. Supports both RFC 3164 and RFC 5424 Syslog standards as well as UDP and encrypted TCP transports.

Please note that a lot of this functionality is still untested from a COM+ environment. I developed and used the COM interop for my own use, and haven't used or tested all possible configurations.

Any additions are very welcome, please submit a pull request/merge request if you want to contribute.

### Installation

You can either compile the SyslogNet.client.dll yourself using Visual Studio, or use the precompiled dll and its dependencies, found in the `Precompiled DLLs` folder.

Copy the dll and its dependencies to a directory somewhere the application pool of your classic ASP application has rights. Then register the main SyslogNet.client.dll. The COM+ dll is 64 bit, so it requires the 64-bit regasm.exe to install.

`%SystemRoot%\Microsoft.NET\Framework64\v4.0.30319\regasm SyslogNet.client.dll /tlb:SyslogNet.client.dll.tlb /codebase`

Again, make sure the application pool for your ASP site has rights to the directory that contains the dll.

### Usage

In ASP the client can be used like this:

```visual-basic
<% 
Set Syslog = Server.CreateObject("SyslogClient")
    Syslog.Open("udp://localhost:514")
    call Syslog.Send5424(1, 3, "ASP", "APP", "This is a simple 5424 test to localhost")
    call Syslog.Send3164(1, 3, "This is a simple 3164 test to localhost")
    Response.Write("Sent Syslog message")
Set Syslog = Nothing
%>
```

### API

`Open(string connectionURI)`

Initializes a connection to the specified syslog server. The connectionstring has a URI format, for example `udp://192.168.1.1:514` for an UDP connection to a host on IP address 192.168.1.1 on port 514.

`string CertificatePath`

Gets or sets a path to a certificat for encryption of syslog messages. This is part of the original SyslogNet, and untested in an ActiveX environment.

`string AppName`

Gets or sets the appname used in the syslog messages.

`string Host`

Gets the current host to send syslog messages to. Setting the host property is not supported, to use a different host use the `Open()` method.

`string protocol`  

The protocol to use, either "udp" or "tcp"

`int Port`

The port to use

`Send5424(int facility, int severity, string procid, string msgid, string message)`

Sends a syslog message in RFC5424 format

`Send3164(int facility, int severity, string message)`

Sends a syslog message in RFC3164 format 

- *facility*
  Needs to be one of these (integer) values;
  
          KernelMessages = 0,
          UserLevelMessages = 1,
          MailSystem = 2,
          SystemDaemons = 3,
          SecurityOrAuthorizationMessages1 = 4,
          InternalMessages = 5,
          LinePrinterSubsystem = 6,
          NetworkNewsSubsystem = 7,
          UUCPSubsystem = 8,
          ClockDaemon1 = 9,
          SecurityOrAuthorizationMessages2 = 10,
          FTPDaemon = 11,
          NTPSubsystem = 12,
          LogAudit = 13,
          LogAlert = 14,
          ClockDaemon2 = 15,
          LocalUse0 = 16,
          LocalUse1 = 17,
          LocalUse2 = 18,
          LocalUse3 = 19,
          LocalUse4 = 20,
          LocalUse5 = 21,
          LocalUse6 = 22,
          LocalUse7 = 23

- *severity*
  Needs to be one of these (integer) values;
  
          Emergency = 0,
          Alert = 1,
          Critical = 2,
          Error = 3,
          Warning = 4,
          Notice = 5,
          Informational = 6,
          Debug = 7,

- *procId (Send5424() only)* 
  
  The process name or process id, to group or identify specific processes.

- *msgId (Send5424() only)* 
  msgid should identify the type of message. Messages with the same MSGID should reflect events of the same semantics.

- *message*
  Free text message you want to send

### License

Copyright (c) 2019 Erik Oosterwaal

This wrapper uses the SyslogNet syslog client, which is licensed under the MIT License (MIT). More information can be found here: [https://github.com/emertechie/SyslogNet](https://github.com/emertechie/SyslogNet)

The software is licensed under the MIT License (MIT). See licence.md for details.
